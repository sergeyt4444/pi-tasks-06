#pragma once
#include <string>
#include <iostream>
using namespace::std;

class Student;
class Group
{
private:
	string Title;
	int num;
	Student* head;
	Student* staff[30];
public:
	Group(void);
	Group(Group& gr);
	Group(string _Title, int _num) ;
	string GetTitle (void);
	void SetTitle (string _Title);
	int GetNum (void);
	void SetNum (int _num);
	void AddStud(Student* st);
	int FindStud (int _ID);
	Student* RemoveStud(int _ID);
	void SetHead (Student* _head);
	int GetHead (void);
	int getAvMark (void);
	~Group(void);
};

