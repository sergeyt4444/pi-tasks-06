#include "Group.h"
#include "Student.h"
#include <string>
#include <iostream>
using namespace::std;



Group::Group(void): num (0)
{
	Title = "";
	head = new Student;
	staff[100] = new Student[100];
}

Group::Group (string _Title, int _num) : Title(_Title), num(0)
{
}

string Group:: GetTitle (void)
{
	return (Title);
}

void Group:: SetTitle (string _Title)
{
	Title = _Title;
}

int Group:: GetNum (void)
{
	return num;
}


void Group:: AddStud(Student* st)
{
	num++;
	staff[num] = st;
}

int Group:: FindStud (int _ID)
{
	int stnum=-1;
	for (int i=0; i<num; i++)
		if (staff[i]->GetID() == _ID)
		{
			stnum = i;
			break;
		}
	return stnum;
}

Student* Group:: RemoveStud(int _ID)
{
	int removedst = FindStud (_ID);
	Student* st = new Student;
	staff[removedst] = staff[num];
	num--;
	return st;
}

void Group::  SetHead (Student* _head)
{
	head = _head;
}

int Group:: GetHead (void)
{
	return head->GetID();
}

int Group:: getAvMark (void)
{
	int avMark=0;
	int number;
	for (int i = 0; i<num; i++)
	{
		number++;
		avMark += staff[i]->getAvMark();
	}
	return avMark/number;
}


Group::~Group(void)
{
	Title = ' ';
	num = 0;
	delete head;
	delete [] staff;
}
