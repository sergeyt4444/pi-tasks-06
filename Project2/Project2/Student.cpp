#include "Student.h"
#include "Group.h"
#include <string>
#include <iostream>
using namespace::std;
Student::Student(void):ID(0), num(0)
{
	Fio = "";
	marks = new int [10];
	for(int i=0; i<10;i++)
		marks[i]=0;
	group = new Group;
}
Student::Student (int _ID, int _num, string _Fio, Group* _group): ID(_ID), Fio(_Fio), num(_num)
{
	group = new Group;
	group= _group;
}

Student::Student (Student& st)
{
	ID = st.ID;
	Fio = st.Fio;
	num = st.num;
	group = st.group;
	for (int i = 0; i<10; i++)
		marks[i] = st.marks[i];
}
int Student:: GetID(void)
{
	return ID;
}
int Student:: getNum(void)
{    return num;     }  
string Student:: getFio(void)
{
	return Fio;
}
const Group* Student:: getGroup(void)
{
	return group;
}
void Student:: getMarks(int* mark)
{
	for(int i = 0; i<10; i++)
		mark[i] = marks [i];
}
void Student:: AddMarks(int* mark)
{
	for (int i=0; i<10; i++)
		marks[i]=mark[i];
}
void Student:: SetGroup(Group* _group)
{
	group= _group;
}
int Student:: getAvMark()
{
	int n= 0;
	int i;
	for(i = 1; i<10; i++)
		n+=marks[i];
	n/=i;
	return n;
}

Student::~Student(void)
{
	ID = 0;
	num = 0;
	Fio =' ';
	delete [] marks;
	delete group;
}
