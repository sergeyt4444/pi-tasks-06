#pragma once
#include <iostream>
#include <string>
using namespace::std;

class Group;
class Student
{
private:
	int ID;
	string Fio;
	int num;
	int* marks;
	Group* group;
public:
	Student(void);
	Student (int _ID, int _num, string _Fio, Group* _group);
	Student (Student& st);
	int GetID(void);
	int getNum(void);
	string getFio(void);
	const Group* getGroup(void);
	void getMarks(int* mark);
	void AddMarks(int* mark);
	void SetGroup(Group* _group);
	int getAvMark();
	~Student(void);
};

